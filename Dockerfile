FROM tomcat:8
RUN apt-get update && apt-get install -y jq python3 python3-pip python3-dateutil postgresql-client libxml2-utils redis-tools locales && apt-get clean
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8
RUN python3 -m pip install pytz
# IRIS WSS
COPY deps/webserviceshell-2.4.9.war /usr/local/tomcat/webapps/fdsnws#station#1.war
RUN mkdir -p /home/sysop/work/requests
RUN mkdir -p /home/sysop/logs
RUN chown -R 1500:1500 /home/sysop
COPY deps-git/wss-config /home/sysop/wss-config
# Python handler
COPY deps-git/wss-python-handler/WSShandler.py /home/sysop/wss-python-handler/WSShandler-station.py
COPY deps-git/wss-python-handler/ws_spec.py /home/sysop/wss-python-handler/
# Resif Backend
COPY deps-git/wss-resif-backend/ws_backend.py deps-git/wss-resif-backend/ws_requestcache.py /home/sysop/wss-python-handler/
# Data extractor
COPY deps-git/ws-extract-scripts /home/sysop/ws-extract-scripts
EXPOSE 8080
RUN ln -s /home/sysop/logs /root/logs
ENV JAVA_OPTS="-DwssConfigDir='/home/sysop/wss-config'"
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
CMD ["./bin/catalina.sh", "run"]
